# Desafio Front-end

O Desafio consiste na criação de um portal que contemple as seguintes funcionalidades:

- Criação de uma lista de cards para exibir os países mostrando a bandeira, o nome e a capital dele;
- Possibilitar o usuário buscar países;
- Na lista, o usuário pode ir para a página de detalhes do país e ver uma lista mais completa de
informações (bandeira, nome, capital, área, população e top-level domain);
- Criação de um formulário para editar os dados de um país (salvando apenas no client-side);
- Na tela de detalhes do país, adicionar um mapa mostrando a distância entre o país e os 5 países
mais próximos;

# Principais tecnologias e bibliotecas utilizadas

- **create-react-app** para start do projeto;
- **redux** para controle de estado;
- **react-router-dom** para controle de rotas;
- **styled-components** para estilização de componentes;
- **@apollo/client** para requisições GraphQl;
- **leaflet** para apresentação e manipulação de mapa;
- **yup** para validação de formulários;

## O projeto foi desenvolvido em 3 dias corridos, totalizando aproximadamente 24 horas de trabalho.

# Link do projeto publicado

[http://graphcountries.surge.sh/](http://graphcountries.surge.sh/)
