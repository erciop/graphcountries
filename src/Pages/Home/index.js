import React from 'react';
import Card from '../../components/Card';
import Search from '../../components/Search';
import { GlobalArea } from './styled';

const Home = () => {
  return (
    <GlobalArea>
      <Search />
      <Card />
    </GlobalArea>
  );
};

export default Home;
