import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const GlobalArea = styled.div`
  form {
    display: flex;
    flex-direction: column;
    input {
      margin: 5px 10px 0px 10px;
      width: 150px;
      height: 30px;
      padding: 0px 5px;
      border-radius: 4px;
      transition: all 100ms;
      border: 1px solid #999;
      &:focus {
        border: 2px solid #777;
      }
    }
    .label {
      margin-top: 10px;
    }
    span {
      margin-left: 10px;
    }
    .area-buttons {
      margin: 10px;
      margin-top: 20px;
      button {
        color: #fff;
        padding: 10px;
        border-radius: 4px;
        background-color: #003584;
        &:first-child {
          margin-right: 10px;
        }
        &:hover {
          background-color: #024bb7;
        }
      }
    }
  }
`;
