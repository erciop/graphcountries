import React, { useEffect, useRef, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { Form } from '@unform/web';
import { useQuery } from '@apollo/client';
import Input from '../../components/Form/input';
import { GlobalArea } from './styled';
import Api from '../../components/Calls/API';

const EditCountry = () => {
  const history = useHistory();
  const formRef = useRef(null);
  const { name } = useParams();

  const { loading, data, updateQuery } = useQuery(Api.getCountry, {
    variables: { name },
  });

  const [initialData, setInicialData] = useState({
    capital: '',
    area: '',
    populacao: '',
    dominio: '',
  });

  const cancelar = () => {
    history.push(`/${name}`);
  };

  const validar = async dados => {
    formRef.current.setErrors('');
    try {
      const schema = yup.object().shape({
        capital: yup.string().required('O campo é obrigatório'),
        area: yup
          .number()
          .typeError('O campo só aceita valores numéricos')
          .required('O campo é obrigatório'),
        populacao: yup
          .number()
          .typeError('O campo só aceita valores numéricos')
          .required('O campo é obrigatório'),
        dominio: yup.string().required('O campo é obrigatório'),
      });
      await schema.validate(dados, {
        abortEarly: false,
      });
      handleSubmit(dados);
    } catch (err) {
      if (err instanceof yup.ValidationError) {
        const errorMessages = {};
        err.inner.forEach(error => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  const handleSubmit = dados => {
    const { capital, area, populacao } = dados;
    updateQuery(prevData => {
      return {
        Country: [
          {
            ...prevData.Country[0],
            capital,
            area,
            population: populacao,
          },
        ],
      };
    });
    setTimeout(() => {
      history.push(`/${name}`);
    }, 400);
  };

  useEffect(() => {
    if (!loading) {
      const domain = [];
      data.Country[0].topLevelDomains.map(d => domain.push(d.name));
      setInicialData({
        capital: data.Country[0].capital,
        area: data.Country[0].area,
        populacao: data.Country[0].population,
        dominio: domain,
      });
    }
  }, [loading]);

  return (
    <GlobalArea>
      {loading ? (
        <div>carregando...</div>
      ) : (
        <Form
          id="formulario"
          initialData={initialData}
          ref={formRef}
          onSubmit={validar}
        >
          <span className="label">Capital*</span>
          <Input name="capital" id="capital" />
          <span className="label">Area*</span>
          <Input name="area" id="area" />
          <span className="label">População*</span>
          <Input name="populacao" id="populacao" />
          <span className="label">Domínio*</span>
          <Input name="dominio" id="dominio" />
          <div className="area-buttons">
            <button type="submit">Salvar</button>
            <button type="button" onClick={() => cancelar()}>
              Cancelar
            </button>
          </div>
        </Form>
      )}
    </GlobalArea>
  );
};

export default EditCountry;
