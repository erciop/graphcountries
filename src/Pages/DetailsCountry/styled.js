import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const GlobalArea = styled.div`
  margin: 20px;
  .area-conteudo {
    .name-country {
      font-size: 30px;
      margin-bottom: 20px;
    }
    .area-flag {
      margin-bottom: 20px;
      img {
        width: 300px;
      }
    }
    .area-details {
      div {
        margin: 5px 0px;
      }
    }
    .area-buttons {
      margin-top: 20px;
      button {
        color: #fff;
        padding: 10px;
        border-radius: 4px;
        background-color: #003584;
        &:first-child {
          margin-right: 10px;
        }
        &:hover {
          background-color: #024bb7;
        }
      }
    }
    .descricao-mapa {
      margin: 10px 0px;
      font-size: 16px;
    }
    .area-mapa {
      .leaflet-container {
        width: 100%;
        height: 400px;
      }
    }
  }
`;
