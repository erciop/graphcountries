import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import Map from '../../components/Map';
import { GlobalArea } from './styled';
import Api from '../../components/Calls/API';

const DetailsCountry = () => {
  const history = useHistory();
  const { name } = useParams();
  const { loading, data } = useQuery(Api.getCountry, {
    variables: { name },
  });

  const voltar = () => {
    history.push('/');
  };
  const editar = () => {
    history.push(`/${name}/editar`);
  };

  return (
    <GlobalArea>
      {loading ? (
        <div>carregando...</div>
      ) : (
        <div className="area-conteudo">
          <div>
            <div className="name-country">
              {data.Country[0].nameTranslations[0].value}
            </div>
            <div className="area-flag">
              <img src={data.Country[0].flag.svgFile} alt="flag" />
            </div>
            <div className="area-details">
              <div>Capital: {data.Country[0].capital}</div>
              <div>Área: {data.Country[0].area}</div>
              <div>População: {data.Country[0].population}</div>
              <div>
                Domínio:{' '}
                {data.Country[0].topLevelDomains.map(d => (
                  <span style={{ marginRight: '5px' }}>{d.name}</span>
                ))}
              </div>
            </div>
            <div className="area-buttons">
              <button type="button" onClick={() => editar()}>
                Editar
              </button>
              <button type="button" onClick={() => voltar()}>
                Voltar
              </button>
            </div>
          </div>
          <div className="descricao-mapa">
            <div>
              O mapa a seguir apresenta a distância entre{' '}
              {data.Country[0].nameTranslations[0].value} e os 5 países mais
              próximos.
            </div>
            <div style={{ marginTop: '10px', fontSize: '12px' }}>
              *Clique no marcador para obter mais detalhes
            </div>
          </div>
          <div className="area-mapa">
            <Map />
          </div>
        </div>
      )}
    </GlobalArea>
  );
};

export default DetailsCountry;
