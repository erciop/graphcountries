import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const GlobalArea = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  background-color: #003c96;
  font-size: 24px;
  color: #fff;
`;
