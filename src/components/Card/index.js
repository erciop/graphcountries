/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { GlobalArea } from './styled';
import Api from '../Calls/API';

const Card = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const filtroPesquisa = useSelector(
    state => state.FiltroPesquisaReducer.filtroPesquisa,
  );

  const resultFilterNone = useSelector(
    state => state.FiltroPesquisaReducer.resultFilterNone,
  );

  const { loading, data } = useQuery(Api.getCountries);
  console.log(data);

  const [resultArray, setResultArray] = useState('');

  useEffect(() => {
    if (!loading) {
      dispatch({
        type: 'SET_RESULT_API',
        payload: data.Country,
      });
      setResultArray(data.Country);
    }
  }, [data]);

  useEffect(() => {
    const constructCard = value => {
      if (value.length !== 0) {
        setResultArray(value);
      }
    };
    if (filtroPesquisa.length === 0) {
      constructCard(resultArray);
    } else {
      constructCard(filtroPesquisa);
    }
  }, [filtroPesquisa]);

  const visualizarDetalhes = name => {
    dispatch({
      type: 'SET_NENHUM_RESULTADO',
      payload: false,
    });
    dispatch({
      type: 'FILTRO_DIGITADO',
      payload: '',
    });
    dispatch({
      type: 'SET_FILTRAR',
      payload: [],
    });
    history.push(name);
  };
  return (
    <GlobalArea>
      {resultArray === '' ? (
        <div>carregando...</div>
      ) : (
        <>
          {resultFilterNone ? (
            <div>Nenhum país encontrado</div>
          ) : (
            resultArray.map(item => (
              <button
                key={item._id}
                className="container"
                type="button"
                onClick={() => visualizarDetalhes(item.name)}
              >
                <div className="name-country">
                  {item.nameTranslations[0].value}
                </div>
                <div className="area-flag">
                  <img src={item.flag.svgFile} alt="flag" />
                </div>
                <div>Capital: {item.capital}</div>
              </button>
            ))
          )}
        </>
      )}
    </GlobalArea>
  );
};

export default Card;
