import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const GlobalArea = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  .container {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    margin: 10px;
    padding: 10px;
    border-radius: 5px;
    width: 200px;
    height: 150px;
    background-color: #fff;
    color: #222;
    box-shadow: 1px 1px 4px 1px #ccc;
    transition: all 100ms;
    &:hover {
      transform: scale(1.05);
    }
    .name-country {
      display: flex;
      justify-content: center;
      font-size: 18px;
    }
    .area-flag {
      display: flex;
      justify-content: center;
      img {
        width: 100px;
      }
    }
  }
`;
