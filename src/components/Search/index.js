import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AreaFiltrosPesquisar } from './styled';

const PesquisarFiltros = () => {
  const dispatch = useDispatch();
  const resultFilterNone = useSelector(
    state => state.FiltroPesquisaReducer.resultFilterNone,
  );
  const filtroDigitado = useSelector(
    state => state.FiltroPesquisaReducer.filtroDigitado,
  );
  const resultApi = useSelector(state => state.ResultApiReducer.resultApi);

  const submitFiltro = event => {
    if (event !== undefined) {
      event.preventDefault();
    }
    if (resultFilterNone) {
      dispatch({
        type: 'SET_NENHUM_RESULTADO',
        payload: false,
      });
    }
    if (filtroDigitado !== '') {
      const filtro = resultApi.filter(
        i =>
          i.nameTranslations[0].value
            .toLowerCase()
            .indexOf(filtroDigitado.toLowerCase()) > -1,
      );
      if (filtro.length === 0) {
        dispatch({
          type: 'SET_NENHUM_RESULTADO',
          payload: true,
        });
      } else {
        dispatch({
          type: 'SET_FILTRAR',
          payload: filtro,
        });
      }
    }
  };

  const limpaFiltroNome = () => {
    dispatch({
      type: 'SET_NENHUM_RESULTADO',
      payload: false,
    });
    dispatch({
      type: 'FILTRO_DIGITADO',
      payload: '',
    });
    dispatch({
      type: 'SET_FILTRAR',
      payload: resultApi,
    });
    document.getElementById('filtroNome').reset();
  };

  const changeFiltro = event => {
    if (event.target.value === '') {
      limpaFiltroNome();
    } else {
      dispatch({
        type: 'FILTRO_DIGITADO',
        payload: event.target.value,
      });
    }
  };

  return (
    <AreaFiltrosPesquisar>
      <form
        onSubmit={submitFiltro}
        id="filtroNome"
        className="area-input-pesquisar"
      >
        <input
          className="pesquisar"
          placeholder="Filtrar por nome do país..."
          type="text"
          onChange={changeFiltro}
          defaultValue={filtroDigitado}
          id="pesquisaNome"
        />
      </form>
      {filtroDigitado !== '' && (
        <button
          form="filtroNome"
          type="button"
          onClick={limpaFiltroNome}
          className="limpar-filtro-nome"
        >
          X
        </button>
      )}
      <button onClick={submitFiltro} type="submit" className="button-pesquisar">
        buscar
      </button>
    </AreaFiltrosPesquisar>
  );
};

export default PesquisarFiltros;
