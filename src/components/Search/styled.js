import styled from 'styled-components';
// eslint-disable-next-line import/prefer-default-export
export const AreaFiltrosPesquisar = styled.div`
  display: flex;
  width: 100%;
  margin: 10px;
  .area-input-pesquisar {
    width: 100%;
    .pesquisar {
      width: 100%;
      padding-left: 10px;
      padding-right: 10px;
      height: 40px;
      margin-top: 10px;
      border: 1px solid #ccc;
      border-right: 0px;
      border-radius: 4px 0px 0px 4px;
    }
  }

  .pesquisar::-webkit-input-placeholder {
    color: #b7b7b7;
    font-size: 16px;
  }
  .button-pesquisar {
    display: flex;
    margin: 10px 0px 0px 0px;
    padding: 0px 10px;
    height: 40px;
    background-color: #fff;
    border-radius: 0px 4px 4px 0px;
    border: 1px solid #ccc;
    align-items: center;
  }
  .limpar-filtro-nome {
    display: flex;
    margin: 10px 0px 0px 0px;
    padding: 0px 10px;
    height: 40px;
    background-color: #fff;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    align-items: center;
    font-size: 20px;
    font-weight: 700;
    color: #757575;
  }
`;
