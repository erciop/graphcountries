import { gql } from 'apollo-boost';

// eslint-disable-next-line import/prefer-default-export
const API = {
  getCountries: gql`
    query {
      Country {
        name
        _id
        flag {
          svgFile
        }
        nameTranslations(filter: { languageCode_in: ["br"] }) {
          value
        }
        capital
        location {
          latitude
          longitude
        }
      }
    }
  `,
  getCountry: gql`
    query($name: String) {
      Country(name: $name) {
        name
        flag {
          svgFile
        }
        nameTranslations(filter: { languageCode_in: ["br"] }) {
          value
        }
        capital
        area
        population
        topLevelDomains {
          name
        }
      }
    }
  `,
  editCountry: gql`
    query($name: String) {
      Country(name: $name) {
        name
        flag {
          svgFile
        }
        nameTranslations(filter: { languageCode_in: ["br"] }) {
          value
        }
        capital
        area
        population
        topLevelDomains {
          name
        }
      }
    }
  `,
  getDistance: gql`
    query {
      Country {
        name
        location {
          latitude
          longitude
        }
        nameTranslations(filter: { languageCode_in: ["br"] }) {
          value
        }
        distanceToOtherCountries(first: 5) {
          distanceInKm
          countryName
        }
      }
    }
  `,
};

export default API;
