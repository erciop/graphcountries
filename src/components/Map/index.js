import React, { useState, useEffect } from 'react';
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  Polyline,
} from 'react-leaflet';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import Api from '../Calls/API';

const limeOptions = { color: 'lime' };

const Map = () => {
  const { name } = useParams();
  const { loading, data } = useQuery(Api.getDistance);
  const [paisPrincipal, setPaisPrincipal] = useState([]);
  const [coordenadas, setCoordenadas] = useState([]);
  const [markerPaisesVizinho, setMarkerPaisesVizinhos] = useState([]);

  const resultApi = useSelector(state => state.ResultApiReducer.resultApi);
  useEffect(() => {
    const paisesVizinhos = [];
    const paisesVizinhosCoordenadas = [];
    const paisesVizinhosMarker = [];
    if (!loading) {
      const filtrarPais = data.Country.filter(i => i.name === name);

      filtrarPais[0].distanceToOtherCountries.map(i =>
        paisesVizinhos.push([i.countryName, i.distanceInKm]),
      );
      paisesVizinhos.map(i =>
        resultApi.forEach(item => {
          if (item.name === i[0]) {
            paisesVizinhosCoordenadas.push([
              filtrarPais[0].location.latitude,
              filtrarPais[0].location.longitude,
            ]);
            paisesVizinhosCoordenadas.push([
              item.location.latitude,
              item.location.longitude,
            ]);
            paisesVizinhosMarker.push([
              [item.location.latitude, item.location.longitude],
              item.nameTranslations[0].value,
              i[1].toFixed(2),
            ]);
          }
        }),
      );
      setPaisPrincipal([
        [filtrarPais[0].location.latitude, filtrarPais[0].location.longitude],
        filtrarPais[0].nameTranslations[0].value,
      ]);
      setCoordenadas(paisesVizinhosCoordenadas);
      setMarkerPaisesVizinhos(paisesVizinhosMarker);
    }
  }, [loading]);
  return (
    <>
      {loading ? (
        <div>carregando...</div>
      ) : (
        <>
          {paisPrincipal.length !== 0 && (
            <MapContainer
              center={paisPrincipal[0]}
              zoom={3}
              scrollWheelZoom={false}
            >
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Polyline pathOptions={limeOptions} positions={coordenadas} />
              <Marker position={paisPrincipal[0]}>
                <Popup>
                  <div
                    style={{
                      fontWeight: '700',
                    }}
                  >
                    {paisPrincipal[1]}
                  </div>
                </Popup>
              </Marker>
              {markerPaisesVizinho.map(i => (
                <Marker position={i[0]}>
                  <Popup>
                    <div
                      style={{
                        textAlign: 'center',
                        fontWeight: '700',
                        marginBottom: '5px',
                      }}
                    >
                      {i[1]}
                    </div>
                    <div>
                      Distância entre {i[1]} e {paisPrincipal[1]}: {i[2]}km
                    </div>
                  </Popup>
                </Marker>
              ))}
            </MapContainer>
          )}
        </>
      )}
    </>
  );
};

export default Map;
