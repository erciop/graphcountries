import { ApolloClient, InMemoryCache } from '@apollo/client';

// eslint-disable-next-line import/prefer-default-export
export const client = new ApolloClient({
  uri: 'https://countries-274616.ew.r.appspot.com/',
  cache: new InMemoryCache(),
});
