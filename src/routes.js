import React from 'react';
import { Switch, Route } from 'react-router-dom';
import DetailsCountry from './Pages/DetailsCountry';
import EditCountry from './Pages/EditCountry';
import Home from './Pages/Home';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/:name" exact component={DetailsCountry} />
      <Route path="/:name/editar" exact component={EditCountry} />
    </Switch>
  );
}
