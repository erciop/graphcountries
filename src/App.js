import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { client } from './config/client-graphql';
import Header from './components/Header';
import GlobalStyle from './globalstyled';
import Routes from './routes';

function App() {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <Header />
        <Routes />
        <GlobalStyle />
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
