import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Card from '../../components/Card';
import Reducers from '../../reducers';
import API from '../../components/Calls/API';

const store = createStore(Reducers);

const mocks = [
  {
    request: {
      query: API.getCountries,
    },
    result: {
      data: {
        Country: [
          {
            name: 'Afghanistan',
            _id: '3',
            capital: 'Kabul',
            flag: { svgFile: 'https://restcountries.eu/data/afg.svg' },
            location: { latitude: 33, longitude: 65 },
            nameTranslations: [
              {
                value: 'Afeganistão',
              },
            ],
          },
        ],
      },
    },
  },
];

describe('Card', () => {
  it('deve renderizar os cards', () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <Card />
        </Provider>
      </MockedProvider>,
    );
  });
});
