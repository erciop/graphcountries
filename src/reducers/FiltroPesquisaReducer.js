const initialState = {
  filtroPesquisa: [],
  resultFilterNone: false,
  filtroDigitado: '',
};

const FiltroPesquisaReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_FILTRAR':
      return { ...state, filtroPesquisa: action.payload };
    case 'SET_NENHUM_RESULTADO':
      return { ...state, resultFilterNone: action.payload };
    case 'FILTRO_DIGITADO':
      return { ...state, filtroDigitado: action.payload };
    default:
      return state;
  }
};

export default FiltroPesquisaReducer;
