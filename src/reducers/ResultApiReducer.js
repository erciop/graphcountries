const initialState = {
  resultApi: [],
};

const ResultApiReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_RESULT_API':
      return { ...state, resultApi: action.payload };
    default:
      return state;
  }
};

export default ResultApiReducer;
