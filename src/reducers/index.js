import { combineReducers } from 'redux';
import FiltroPesquisaReducer from './FiltroPesquisaReducer';
import ResultApiReducer from './ResultApiReducer';

export default combineReducers({
  ResultApiReducer,
  FiltroPesquisaReducer,
});
